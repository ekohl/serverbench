package dev.lion7

import io.helidon.webserver.WebServer
import io.helidon.webserver.http.Handler

fun main() {
    WebServer.builder().port(8080).routing { router ->
        router.get("/bench/{count}", Handler { request, response ->
            val count = request.path().pathParameters().get("count").toInt()
            val result = Fibonacci.iterative(count)
            response.send(result.toString())
        })
    }.build().start()
}
