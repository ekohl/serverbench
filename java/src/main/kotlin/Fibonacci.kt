package dev.lion7

import java.math.BigInteger

object Fibonacci {
    /**
     * Inspired by https://www.baeldung.com/java-fibonacci#1-recursive-method
     *
     * Very slow > 40, do not use this!
     */
    fun recursive(n: Int): Long {
        if (n == 1 || n == 0) {
            return n.toLong()
        }
        return recursive(n - 1) + recursive(n - 2)
    }

    /**
     * Inspired by https://www.baeldung.com/java-fibonacci#2-iterative-method
     */
    fun iterative(n: Int): BigInteger {
        when (n) {
            0 -> BigInteger.ZERO
            1 -> BigInteger.ONE
        }
        var n0 = BigInteger.ZERO
        var n1 = BigInteger.ONE
        var tempNthTerm: BigInteger
        for (i in 2..n) {
            tempNthTerm = n0 + n1
            n0 = n1
            n1 = tempNthTerm
        }
        return n1
    }
}
